### The Matrix Cat bot.
He can send gif or img with cat using [CATAAS](https://cataas.com).

#### Setup

First clone repo and create `.env` and `config.toml`

Examples:

`.env`
```dotenv
USERNAME=@catbot:example.com
PASSWORD=password
SERVER=https://example.com
```

[allowlist and blocklist formats](https://simple-matrix-bot-lib.readthedocs.io/en/latest/examples.html#id2)

`config.toml`
```toml
[simplematrixbotlib.config]
allowlist = []
blocklist = []
admin_id = '@admin:example.com'
lang = 'ua'
```

Then

```shell
docker compose up -d --build
```

#### Commands
Bot can work in encrypted rooms.

Please read: [E2E requirements](https://simple-matrix-bot-lib.readthedocs.io/en/latest/manual.html#requirements) and [Interactive SAS verification using Emoji](https://simple-matrix-bot-lib.readthedocs.io/en/latest/manual.html#interactive-sas-verification-using-emoji)

- `!catbot_help` - sends help
- `!catbot_img` - sends cat image
- `!catbot_gif` - sends cat animation
- `!catbot_says text` - sends cat image with text