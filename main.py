from dataclasses import dataclass
from environs import Env
import simplematrixbotlib as botlib
from handlers import setup
import gettext

env = Env()
env.read_env()

USERNAME = env('USERNAME')
PASSWORD = env('PASSWORD')
SERVER = env('SERVER')


@dataclass
class MyConfig(botlib.Config):
    _admin_id: str = 'unknown'
    _lang: str = 'en'

    @property
    def admin_id(self) -> str:
        return self._admin_id

    @admin_id.setter
    def admin_id(self, value: str) -> None:
        self._admin_id = value

    @property
    def lang(self) -> str:
        return self._lang

    @lang.setter
    def lang(self, value: str) -> None:
        self._lang = value


config = MyConfig()
config.load_toml('config.toml')
config.encryption_enabled = True
config.emoji_verify = True
config.ignore_unverified_devices = True
config.store_path = './crypto_store/'

creds = botlib.Creds(SERVER, USERNAME, PASSWORD)
bot = botlib.Bot(creds, config)
PREFIX = '!catbot_'

# Localization
_ = gettext.gettext

setup(bot, prefix=PREFIX)
bot.run()
