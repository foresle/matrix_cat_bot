import gettext
import simplematrixbotlib as botlib


async def help_handler(room_id: str, bot: botlib.Bot, sender: str, admin_id: str):

    lang = gettext.translation('help', localedir='locales', languages=(bot.config.lang,))
    lang.install()
    _ = lang.gettext

    msg: str = _(
        'Hi **{}**\n\n'
        'Prefix: `!catbot_`\n'
        'Commands:\n'
        '- `img` - sends random cat img\n'
        '- `gif` - sends random cat gif\n'
        '- `says text` - sends random cat with text\n'
        'Example: `!catbot_img`\n'
        'Admin contact: **{}**'
    )

    await bot.api.send_markdown_message(room_id=room_id, message=msg.format(sender, admin_id))