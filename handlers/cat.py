import simplematrixbotlib as botlib
import aiohttp


async def cat_img_handler(room_id: str, bot: botlib.Bot, _):
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get('https://cataas.com/cat') as response:
                open('cat.jpg', 'wb').write(await response.read())
    except aiohttp.ClientError:
        msg: str = _('Your cat is lost :(')

        await bot.api.send_markdown_message(room_id=room_id, message=msg)
        return None

    await bot.api.send_image_message(room_id, image_filepath='cat.jpg')


async def cat_gif_handler(room_id: str, bot: botlib.Bot, _):
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get('https://cataas.com/cat/gif') as response:
                open('cat.gif', 'wb').write(await response.read())
    except aiohttp.ClientError:
        msg: str = _('Your cat is lost :(')

        await bot.api.send_markdown_message(room_id=room_id, message=msg)
        return None

    await bot.api.send_image_message(room_id, image_filepath='cat.gif')


async def cat_says_handler(room_id: str, bot: botlib.Bot, text: str, _):
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(f'https://cataas.com/cat/says/{text}') as response:
                open('cat.jpg', 'wb').write(await response.read())
    except aiohttp.ClientError:
        msg: str = _('Your cat is lost :(')

        await bot.api.send_markdown_message(room_id=room_id, message=msg)
        return None

    await bot.api.send_image_message(room_id, image_filepath='cat.jpg')
