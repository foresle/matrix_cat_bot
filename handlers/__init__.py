import gettext
import urllib.parse
import nio
import simplematrixbotlib as botlib

from .help import help_handler
from .cat import cat_img_handler, cat_gif_handler, cat_says_handler


def setup(bot: botlib.Bot, prefix: str):
    @bot.listener.on_message_event
    async def help_command(room: nio.rooms.MatrixRoom, message: nio.events.room_events.Event):
        match = botlib.MessageMatch(room, message, bot, prefix=prefix)

        if match.is_not_from_this_bot() and match.prefix() and match.command('help'):
            await help_handler(bot=bot, room_id=room.room_id, sender=message.sender, admin_id=bot.config.admin_id)

    # Cats
    lang = gettext.translation('cat', localedir='locales', languages=(bot.config.lang,))
    lang.install()
    _cat_loc = lang.gettext

    @bot.listener.on_message_event
    async def cat_img_command(room: nio.rooms.MatrixRoom, message: nio.events.room_events.Event):
        match = botlib.MessageMatch(room, message, bot, prefix=prefix)

        if match.is_not_from_this_bot() and match.is_from_allowed_user() and match.prefix() and match.command(
                'img'):
            await cat_img_handler(bot=bot, room_id=room.room_id, _=_cat_loc)

    @bot.listener.on_message_event
    async def cat_gif_command(room: nio.rooms.MatrixRoom, message: nio.events.room_events.Event):
        match = botlib.MessageMatch(room, message, bot, prefix=prefix)

        if match.is_not_from_this_bot() and match.is_from_allowed_user() and match.prefix() and match.command(
                'gif'):
            await cat_gif_handler(bot=bot, room_id=room.room_id, _=_cat_loc)

    @bot.listener.on_message_event
    async def cat_says_command(room: nio.rooms.MatrixRoom, message: nio.events.room_events.Event):
        match = botlib.MessageMatch(room, message, bot, prefix=prefix)

        if match.is_not_from_this_bot() and match.is_from_allowed_user() and match.prefix() and match.command(
                'says'):
            await cat_says_handler(bot=bot, room_id=room.room_id, text=urllib.parse.quote(' '.join(match.args())), _=_cat_loc)
